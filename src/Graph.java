import java.util.*;


public class Graph<Content> implements Iterable<Edge>{
    // classe de graphe non orientés permettant de manipuler
    // en même temps des arcs (orientés)
    // pour pouvoir stocker un arbre couvrant, en plus du graphe
    
	int order;
	int edgeCardinality;

	ArrayList<Content> m_contenu_sommets;
	ArrayList<LinkedList<Edge>> adjacency;
	ArrayList<LinkedList<Arc>> inAdjacency;
	ArrayList<LinkedList<Arc>> outAdjacency;
	
	public boolean isVertex(int index)
	{
	    return false;
	}
	
	public <T> ArrayList<LinkedList<T>> makeList(int size) {
		ArrayList<LinkedList<T>> res = new ArrayList<>(size);
		for(int i = 0; i <= size; i++) {
			res.add(new LinkedList<>());
		}
		return res;
	}
	
	public Graph(int upperBound) {
	     adjacency = this.<Edge>makeList(upperBound);
	}
	
	public void addVertex(int indexVertex) {
	    adjacency.add(indexVertex, null);
	}

	public void addArc(Arc arc) {
	    // à remplir
	}
	
	public void addEdge(Edge e) {
		adjacency.get(e.getSource()).add(e);
		adjacency.get(e.getDest()).add(e);
	}

	public ArrayList<Arc> outNeighbours(int sommet)
	{
		return null;
	}

	public ArrayList<Integer> getNeighbours(int sommet)
	{
		var neighbours = new ArrayList<Integer>();
		for(var neighbour : adjacency.get(sommet))
			neighbours.add(neighbour.getDest());
		return  neighbours;
	}

	public int getNumbersOfVertices()
	{
		return adjacency.size();
	}

	public Edge getEdge(int sommet_dep,int sommet_arriv)
	{
		assert est_connecte(sommet_dep, sommet_arriv) : "Erreur pas arrete !";

		for(var edge : adjacency.get(sommet_dep))
		{
			if(edge.getDest() == sommet_arriv)
				return edge;
		}
		assert false : "Erreur : Never come here !";
		return null;
	}

	public boolean est_connecte(int sommet_dep, int sommet_arr)
	{
		assert sommet_arr >= 0 && sommet_dep >= 0 && sommet_arr <= adjacency.size() && sommet_dep <= adjacency.size(): "erreur borne !" ;
		for(var edge : adjacency.get(sommet_dep))
		{
			if(edge.getDest() == sommet_arr)
				return true;
		}
		return false;
	}

	public void ajouter_contenu_a_sommet(int sommet, Content contenu)
	{
		m_contenu_sommets.add(sommet,contenu);
	}

	public Content get_contenu_du_sommet(int sommet)
	{
		return m_contenu_sommets.get(sommet);
	}

	@Override
	public Iterator<Edge> iterator() {
		return null;
	}
}
